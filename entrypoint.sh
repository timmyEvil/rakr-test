#!/bin/bash
sed -i -e 's/caddy_frontend_url/'${CADDY_FRONTEND_URL}'/g' /Caddyfile
sed -i -e 's/caddy_backend_url/'${CADDY_BACKEND_URL}'/g' /Caddyfile
nohup java -jar /app.jar --cht.issue.server.url=${CHT_ISSUE_SERVER_URL} --spring.datasource.url=${SPRING_DS_URL} --cht.sentry.webapi-entry=${CHT_SENTRY_WEBAPI_ENTRY} &
./caddy

