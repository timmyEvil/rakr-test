FROM mcpayment/ubuntu-java8
VOLUME /tmp
ADD issue-app-0.1.0-SNAPSHOT-bootable.jar app.jar
COPY caddy /caddy
COPY Caddyfile /Caddyfile
COPY dist.tar.gz /dist.tar.gz
COPY entrypoint.sh /entrypoint.sh
RUN tar -zxvf /dist.tar.gz
RUN chmod 755 /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
